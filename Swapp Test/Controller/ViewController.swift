//
//  ViewController.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var logTitle: String!
    var userCV: UICollectionView!
    var userModel: [UserModel]!
    var nav: NavigationViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLayout()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Log
        logTitle = "(ViewController - viewDidLoad)"
        print("\(String(describing: logTitle!)) started.")

        // Get All Users
        nav.showLoading(bool: true)
        userModel = [UserModel]()
        ApiCore.shared.getApi(urlString: Theme.shared.userUrl) { (data, success) in
            if success {
                do {
                    self.userModel = try JSONDecoder().decode([UserModel].self, from: data)
                    
                    // Listing All Users
                    DispatchQueue.main.async {
                        print("\(String(describing: self.logTitle!)) userCV Reloaded")
                        self.userCV.reloadData()
                        self.nav.showLoading(bool: false)
                    }
                    
                } catch let err {
                    print(" Json Error : \(err)")
                }
            } else {
                
                // Network Error Handling
                self.nav.showLoading(bool: false)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Reloading User List on Device Orientation Change
        guard let flowLayout = userCV.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        userCV.reloadData()
        flowLayout.invalidateLayout()
        view.setNeedsLayout()
    }

}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AlbumViewDelegate {
    
    /**
     Controller Resumed
     ````
     Called by the returning ViewController
     ````
     */
    func controllerResumed() {
        let nav = self.navigationController as! NavigationViewController
        nav.leftButton.isHidden = true
    }
    
    // Number of Rows
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userModel.count
    }
    
    // Cell Items Loaded
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "user", for: indexPath) as! UserCollectionViewCell
        
            cell.userImage.maskCircle(anyImage: UIImage(named: "default_user")!)
            cell.userName.text = userModel[indexPath.row].name
            cell.userCity.text = userModel[indexPath.row].address.city
            cell.userEmail.text = userModel[indexPath.row].email
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = Theme.shared.secondaryColorDark
        } else {
            cell.backgroundColor = Theme.shared.secondaryColor
        }
        
        return cell
    }
    
    // Cell Sizes
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight {
            return CGSize(width: view.frame.width, height: view.frame.height/4)
        }
        return CGSize(width: view.frame.width, height: view.frame.height/8)
    }
    
    // Cell Spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    // Cell Selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextVC = AlbumViewController()
        nextVC.userId = userModel[indexPath.row].id
        nextVC.delegate = self
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}



