//
//  AlbumModel.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import Foundation

struct AlbumModel: Decodable {
    let userId: Int
    let id: Int
    let title: String
}
