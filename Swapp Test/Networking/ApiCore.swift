//
//  ApiCore.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import Foundation

class ApiCore {
    
    static let shared = ApiCore()
    let session = URLSession(configuration: .default)
    var logTitle: String!
    
    /**
     Get API
     ````
     GET - API caller
     ````
     */
    func getApi(urlString: String, completion: @escaping (Data, Bool)->()) {
        logTitle = "|ApiCore - getApi|"
        let url = URL(string: urlString)
        
        print("\(logTitle!) Api Called : \(urlString)")
        let dataTask = session.dataTask(with: url!) { (data, response, err) in
            if let data=data, let response=response as? HTTPURLResponse, response.statusCode==200 {
                print("\(self.logTitle!) Api Responded : \(urlString)")
                completion(data, true)
            } else {
                print("\(self.logTitle!) Api Error : Url :: \(urlString) \t Err :: \(String(describing: err))")
                completion(Data.init(), false)
            }
        }
        dataTask.resume()
        
    }
    
}
