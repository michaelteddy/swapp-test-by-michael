//
//  AlbumPicsCollectionViewCell.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

class AlbumPicsCollectionViewCell: UICollectionViewCell {
    
    var title: UILabel!
    var albumImage: CustomImageView!
    var visualEV: UIVisualEffectView!
    private var customConstraints = [NSLayoutConstraint]()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        title = UILabel()
        title.translatesAutoresizingMaskIntoConstraints = false
        title.font = UIFont.boldSystemFont(ofSize: 14)
        title.textAlignment = .center
        title.numberOfLines = 2
        title.textColor = Theme.shared.primaryColor
        
        albumImage = CustomImageView(image: UIImage(named: "default_album")?.withRenderingMode(.alwaysTemplate))
        albumImage.tintColor = Theme.shared.textColor
        albumImage.contentMode = .scaleAspectFit
        albumImage.translatesAutoresizingMaskIntoConstraints = false
        
        visualEV = UIVisualEffectView(frame: .zero)
        visualEV.translatesAutoresizingMaskIntoConstraints = false
        visualEV.effect = UIBlurEffect(style: .light)
        visualEV.alpha = 0.6
        
        addSubview(albumImage)
        addSubview(visualEV)
        addSubview(title)
        
        setUpConstraints()
    }
    
    /**
     Setup Constraints
     ````
     All the relevant Constraints are added here
     ````
     */
    func setUpConstraints() {
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": albumImage]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: [], metrics: nil, views: ["v0": albumImage]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": visualEV]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: [], metrics: nil, views: ["v0": visualEV]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]-10-|", options: [], metrics: nil, views: ["v0": title]))
        addConstraint(NSLayoutConstraint.init(item: title, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    /**
     Photo Constraints
     ````
     Constraints specific to Photos list are added here
     ````
     */
    func setUpPhotosConstraints() {
        albumImage.contentMode = .scaleAspectFill
        albumImage.clipsToBounds = true
        visualEV.isHidden = true
        
        title.removeFromSuperview()
        title.textColor = Theme.shared.secondaryColorDark
        title.font = UIFont.systemFont(ofSize: 10)
        addSubview(title)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]-10-|", options: [], metrics: nil, views: ["v0": title]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-5-|", options: [], metrics: nil, views: ["v0": title]))
    }
    
    /**
     Reset Album Constraints
     ````
     Resetting all the original album constraints
     ````
     */
    func resetAlbumConstraints() {
        albumImage.image = UIImage(named: "default_album")?.withRenderingMode(.alwaysTemplate)
        albumImage.contentMode = .scaleAspectFit
        albumImage.clipsToBounds = false
        visualEV.isHidden = false
        
        title.removeFromSuperview()
        title.textColor = Theme.shared.primaryColor
        title.font = UIFont.systemFont(ofSize: 14)
        addSubview(title)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[v0]-10-|", options: [], metrics: nil, views: ["v0": title]))
        addConstraint(NSLayoutConstraint.init(item: title, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    /**
     Clear Constraints
     ````
     Clearing all the constraints
     ````
     */
    private func clearConstraints() {
        customConstraints.forEach { $0.isActive = false }
        customConstraints.removeAll()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
