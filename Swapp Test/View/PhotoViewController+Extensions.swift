//
//  PhotoViewController+Extensions.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

extension PhotoViewController {
    
    /**
     Setup Layout
     ````
     All the layout elements are setup here
     ````
     */
    func setUpLayout() {
        view.backgroundColor = Theme.shared.secondaryColor
        
        // Do any additional setup after loading the view.
        imageView = CustomImageView(frame: .zero)
        imageView.loadImage(imageUrl) {
            self.nav.showLoading(bool: false)
        }
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        
        view.addSubview(imageView)
        
        addConstraints()
    }
    
    /**
     Setup Constraints
     ````
     All the layout constraints are added here
     ````
     */
    func addConstraints() {
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": imageView]))
        view.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    override func viewWillLayoutSubviews() {
        if imageView.image == nil {
            nav.showLoading(bool: true)
        }
        nav.navDelegate = self
        nav.leftButton.isHidden = false
        nav.rightButton.setTitle(Theme.shared.shareTitle, for: .normal)
        
        if UIDevice.current.orientation != .portrait {
            imageView.removeFromSuperview()
            view.addSubview(imageView)
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(Theme.shared.navBarHeight!)-[v0]|", options: [], metrics: nil, views: ["v0": imageView]))
            view.addConstraint(NSLayoutConstraint.init(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        } else {
            imageView.removeFromSuperview()
            view.addSubview(imageView)
            addConstraints()
        }
    }
}
