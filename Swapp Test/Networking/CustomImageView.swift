//
//  ImageLoader.swift
//  LearnIOSFinal
//
//  Created by Michael Fernandez on 21/10/18.
//  Copyright © 2018 Michael Fernandez. All rights reserved.
//

import Foundation
import UIKit

var cache = NSCache<AnyObject, UIImage>()

class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    
    /**
     Load Image
     ````
     Loads and caches images from url
     ````
     */
    func loadImage(_ urlString: String, completion: @escaping ()->()) {
        
        imageUrlString = urlString
        
        image = nil
        guard let url = URL(string: urlString) else {return}
        
        // If in cache, take from cache
        if let img = cache.object(forKey: url as AnyObject) {
            image = img
            return
        }
        
        URLSession.shared.dataTask(with: url  ) {(data, response, err) in
            if let data=data, let response=response as? HTTPURLResponse, response.statusCode==200 {
               
                if err != nil {
                    print("Image Loading Error : \(String(describing:err))")
                    return
                }
                
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data)
                    
                    if self.imageUrlString == urlString {
                        self.image = imageToCache
                    }
                    cache.setObject(imageToCache!, forKey: url as AnyObject)
                    completion()
                }
            }
        }.resume()
    }
}
