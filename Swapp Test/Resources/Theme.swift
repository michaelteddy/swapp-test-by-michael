//
//  Theme.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

class Theme {
    
    static let shared = Theme()
    
    var userUrl: String!
    var albumUrl: String!
    var photosUrl: String!
    
    var primaryColor: UIColor!
    var primaryColorDark: UIColor!
    var secondaryColor: UIColor!
    var secondaryColorDark: UIColor!
    var accentColor: UIColor!
    var textColor: UIColor!
    var navBarHeight: Int!
    var navBarBorderHeight: Int!
    var navLeftButtonSize: CGFloat!
    var navRightButtonSize: CGFloat!
    var userCellIconSize: CGFloat!
    var doneTitle: String!
    var shareTitle: String!
    
    // Keeps all constants
    init() {
        userUrl = "https://jsonplaceholder.typicode.com/users"
        albumUrl = "https://jsonplaceholder.typicode.com/albums?userId="
        photosUrl = "https://jsonplaceholder.typicode.com/photos?albumId="
        primaryColor = UIColor(red:0.94, green:0.42, blue:0.42, alpha:1.0)
        primaryColorDark = UIColor(red:0.86, green:0.39, blue:0.39, alpha:1.0)
        secondaryColor = .white
        secondaryColorDark = UIColor(red:0.97, green:0.97, blue:0.97, alpha:1.0)
        accentColor = UIColor(red:0.47, green:0.42, blue:0.36, alpha:1.0)
        textColor = UIColor(red:0.65, green:0.65, blue:0.65, alpha:1.0)
        navBarHeight = 80
        navBarBorderHeight = 2
        userCellIconSize = 14
        navLeftButtonSize = 28
        navRightButtonSize = 50
        doneTitle = "Done"
        shareTitle = "Share"
    }
}

