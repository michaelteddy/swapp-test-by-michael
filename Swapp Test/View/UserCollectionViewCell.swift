//
//  UserCollectionViewCell.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    
    var userImage: CustomImageView!
    var userName: UILabel!
    var userCity: UILabel!
    var userEmail: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialiseViews()
        
        addViewConstraints()
    }
    
    /**
     Initialise View
     ````
     Initialises all the views
     ````
     */
    func initialiseViews() {
        userImage = CustomImageView(frame: .zero)
        userImage.translatesAutoresizingMaskIntoConstraints = false
        userImage.contentMode = .scaleAspectFill
        userImage.clipsToBounds = true
        
        userName = UILabel()
            userName.textColor = Theme.shared.primaryColorDark
            userName.font = UIFont.boldSystemFont(ofSize: 14)
        userCity = UILabel()
            userCity.textColor = Theme.shared.textColor
            userCity.font = UIFont.systemFont(ofSize: 10)
        userEmail = UILabel()
            userEmail.textColor = Theme.shared.textColor
            userEmail.font = UIFont.systemFont(ofSize: 10)
        
        userName.translatesAutoresizingMaskIntoConstraints = false
        userCity.translatesAutoresizingMaskIntoConstraints = false
        userEmail.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(userImage)
        addSubview(userName)
        addSubview(userCity)
        addSubview(userEmail)
    }
    
    /**
     Add Constraints
     ````
     All relevant constraints are added here
     ````
     */
    func addViewConstraints() {

        let locImageView = UIImageView()
            locImageView.image = UIImage(named: "location")?.withRenderingMode(.alwaysTemplate)
            locImageView.tintColor = Theme.shared.textColor
            locImageView.contentMode = .scaleAspectFit
            locImageView.clipsToBounds = true
        let emailImageView = UIImageView()
            emailImageView.image = UIImage(named: "email")?.withRenderingMode(.alwaysTemplate)
            emailImageView.tintColor = Theme.shared.textColor
            emailImageView.contentMode = .scaleAspectFit
            emailImageView.clipsToBounds = true
        
        locImageView.translatesAutoresizingMaskIntoConstraints = false
        emailImageView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(locImageView)
        addSubview(emailImageView)
        
        // User Image Constraints
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[v0(\(self.frame.height-30))]", options: [], metrics: nil, views: ["v0": userImage]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0(\(self.frame.height-30))]", options: [], metrics: nil, views: ["v0": userImage]))
        addConstraint(NSLayoutConstraint.init(item: userImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        // User Name Constraints
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v1]-20-[v0]-20-|", options: [], metrics: nil, views: ["v0": userName, "v1": userImage]))
        addConstraint(NSLayoutConstraint.init(item: userName, attribute: .top, relatedBy: .equal, toItem: userImage, attribute: .top, multiplier: 1, constant: 10))
        
        // Location Image Icon Constraints
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0(\(Theme.shared.userCellIconSize!))]", options: [], metrics: nil, views: ["v0": locImageView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0(\(Theme.shared.userCellIconSize!))]", options: [], metrics: nil, views: ["v0": locImageView]))
        addConstraint(NSLayoutConstraint.init(item: locImageView, attribute: .top, relatedBy: .equal, toItem: userName, attribute: .bottom, multiplier: 1, constant: 5))
        addConstraint(NSLayoutConstraint.init(item: locImageView, attribute: .leading, relatedBy: .equal, toItem: userName, attribute: .leading, multiplier: 1, constant: 0))
        
        // User City Constraints
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v1]-5-[v0]", options: [], metrics: nil, views: ["v0": userCity, "v1": locImageView]))
        addConstraint(NSLayoutConstraint.init(item: userCity, attribute: .centerY, relatedBy: .equal, toItem: locImageView, attribute: .centerY, multiplier: 1, constant: 0))
        
        // USer Email Icon Constraints
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v1]-10-[v0(\(Theme.shared.userCellIconSize!))]", options: [], metrics: nil, views: ["v0": emailImageView, "v1": userCity]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0(\(Theme.shared.userCellIconSize!))]", options: [], metrics: nil, views: ["v0": emailImageView]))
        addConstraint(NSLayoutConstraint.init(item: emailImageView, attribute: .centerY, relatedBy: .equal, toItem: locImageView, attribute: .centerY, multiplier: 1, constant: 0))
        
        // USer Email Constraints
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v1]-5-[v0]", options: [], metrics: nil, views: ["v0": userEmail, "v1": emailImageView]))
        addConstraint(NSLayoutConstraint.init(item: userEmail, attribute: .centerY, relatedBy: .equal, toItem: emailImageView, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
