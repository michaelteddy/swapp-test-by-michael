//
//  PhotoViewController.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

protocol PhotoViewControllerDelegate {
    func controllerResumed()
}

class PhotoViewController: UIViewController, NavigationViewControllerDelegate {
    
    var imageView: CustomImageView!
    var imageUrl: String!
    var nav: NavigationViewController!
    var delegate: PhotoViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpLayout()
    }
    
    // Left Button Pressed
    func leftButtonPressed() {
        delegate?.controllerResumed()
        self.navigationController?.popViewController(animated: true)
    }
    
    // Right Button Pressed
    func rightButtonPressed() {
        // Share Functionality
        // set up activity view controller
        let imageToShare = [ imageView.image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
