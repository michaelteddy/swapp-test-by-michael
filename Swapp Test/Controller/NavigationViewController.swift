//
//  NavigationViewController.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol NavigationViewControllerDelegate {
    func leftButtonPressed()
    func rightButtonPressed()
}

class NavigationViewController: UINavigationController {
    
    var leftButton: UIButton!
    var rightButton: UIButton!
    var visualEffectView: UIVisualEffectView!
    var container: UIView!
    var imageLogo: UIImageView!
    var border: UIView!
    
    var navDelegate: NavigationViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isHidden = true
        
        // Do any additional setup after loading the view.
        visualEffectView = UIVisualEffectView(frame: .zero)
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false
        visualEffectView.effect = UIBlurEffect(style: .light)
        visualEffectView.alpha = 0.9
        visualEffectView.isHidden = true
        
        createCustomNavBar()
        
        view.addSubview(visualEffectView)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": visualEffectView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|", options: [], metrics: nil, views: ["v0": visualEffectView]))
    }
    
    /**
     Create Custom NavBar
     ````
     This function creates a custom navigation bar with logo, left and right button
     ````
     */
    func createCustomNavBar() {
        container = UIView()
        container.backgroundColor = Theme.shared.accentColor
        container.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(container)
        
        let logo = UIImage(named: "logo")?.withRenderingMode(.alwaysTemplate)
        imageLogo = UIImageView(image: logo)
        imageLogo.tintColor = Theme.shared.secondaryColor
        imageLogo.contentMode = .scaleAspectFit
        imageLogo.translatesAutoresizingMaskIntoConstraints = false
        
        container.addSubview(imageLogo)
        
        border = UIView()
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = Theme.shared.primaryColor
        
        container.addSubview(border)
        
        leftButton = UIButton(type: .system)
        leftButton.setImage(UIImage(named: "back")?.withRenderingMode(.alwaysTemplate), for: .normal)
        leftButton.imageView?.contentMode = .scaleAspectFit
        leftButton.tintColor = Theme.shared.secondaryColorDark
        leftButton.translatesAutoresizingMaskIntoConstraints = false
        leftButton.isHidden = true
        leftButton.addTarget(self.visibleViewController, action: #selector(backClicked(sender:)), for: .touchUpInside)
        
        container.addSubview(leftButton)
        
        rightButton = UIButton(type: .system)
        rightButton.setTitle(Theme.shared.doneTitle, for: .normal)
        rightButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        rightButton.tintColor = Theme.shared.secondaryColorDark
        rightButton.backgroundColor = .clear
        rightButton.translatesAutoresizingMaskIntoConstraints = false
        rightButton.isHidden = true
        rightButton.addTarget(self.visibleViewController, action: #selector(rightButtonClicked(sender:)), for: .touchUpInside)
        
        container.addSubview(rightButton)
        
        setUpConstraints()
    }
    
    /**
     Setup Constraints
     ````
     All the layout constraints are added here
     ````
     */
    func setUpConstraints() {
        // container
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": container]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0(\(Theme.shared.navBarHeight!))]", options: [], metrics: nil, views: ["v0": container]))
    
        // imageLogo
        container.addConstraint(NSLayoutConstraint.init(item: imageLogo, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0))
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[v0]-10-|", options: [], metrics: nil, views: ["v0": imageLogo]))
    
        // border
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": border]))
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0(\(Theme.shared.navBarBorderHeight!))]|", options: [], metrics: nil, views: ["v0": border]))
    
        // leftButton
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[v0(\(Theme.shared.navLeftButtonSize!))]", options: [], metrics: nil, views: ["v0": leftButton]))
        container.addConstraint(NSLayoutConstraint.init(item: leftButton, attribute: .centerY, relatedBy: .equal, toItem: imageLogo, attribute: .centerY, multiplier: 1, constant: 0))
    
        // rightButton
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[v0(\(Theme.shared.navRightButtonSize!))]-20-|", options: [], metrics: nil, views: ["v0": rightButton]))
        container.addConstraint(NSLayoutConstraint.init(item: rightButton, attribute: .centerY, relatedBy: .equal, toItem: imageLogo, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    /**
     Refresh NavBar
     ````
     Recreates Navbar with fresh contents
     ````
     */
    func refreshNavBar() {
        container.removeFromSuperview()
        createCustomNavBar()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // When Back Clicked
    @objc func backClicked(sender: UIButton) {
        navDelegate?.leftButtonPressed()
    }
    
    // When Right Button Clicked
    @objc func rightButtonClicked(sender: UIButton) {
        navDelegate?.rightButtonPressed()
    }
    
    /**
     Toggle NavBar Buttons
     ````
     Toggles the visibility of Navbar buttons
     ````
     */
    func toggleButton() {
        if leftButton.isHidden {
            leftButton.isHidden = false
            rightButton.isHidden = true
        } else {
            leftButton.isHidden = true
            rightButton.isHidden = false
        }
    }
    
    /**
     Show Loading Animation
     ````
     This function shows/hides the loading animation based on the boolean argument
     ````
     */
    func showLoading(bool: Bool) {
        if bool {
            let activityData = ActivityData(type: NVActivityIndicatorType.lineScalePulseOut, color: Theme.shared.primaryColor)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            visualEffectView.isHidden = false
        } else {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            visualEffectView.isHidden = true
        }
    }
}
