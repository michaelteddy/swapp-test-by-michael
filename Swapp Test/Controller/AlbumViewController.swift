//
//  AlbumViewController.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol AlbumViewDelegate {
    func controllerResumed()
}

class AlbumViewController: UIViewController, NavigationViewControllerDelegate, PhotoViewControllerDelegate {

    var userId: Int!
    var albumPicsCV: UICollectionView!
    var albumModel: [AlbumModel]!
    var photosModel: [PhotosModel]!
    var logTitle: String!
    
    var nav: NavigationViewController!
    
    var albumView: Bool!
    
    var delegate: AlbumViewDelegate?
    
    var evenCounter = 0
    var oddCounter = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Log
        logTitle = "(AlbumViewController - viewDidLoad)"
        
        albumView = true
        setUpLayout()
        
        // Initialising Models
        albumModel = [AlbumModel]()
        photosModel = [PhotosModel]()
        
        // Get User Albums
        nav.showLoading(bool: true)
        ApiCore.shared.getApi(urlString: Theme.shared.albumUrl+String(userId)) { (data, success)  in
            if success {
                do {
                    self.albumModel = try JSONDecoder().decode([AlbumModel].self, from: data)
                    
                    // Update User Albums
                    DispatchQueue.main.async {
                        self.albumPicsCV.reloadData()
                        self.nav.showLoading(bool: false)
                    }
                } catch let err {
                    print("\(self.logTitle!) JSON ERROR : \(err)")
                }
            } else {
                // Network Error, Pop ViewController
                self.nav.showLoading(bool: false)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Left Button Pressed
    func leftButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Right Button Pressed
    func rightButtonPressed() {
        albumView = true
        nav.toggleButton()
        albumPicsCV.reloadData()
    }
    
    /**
     Controller Resumed
     ````
     Called by the returning ViewController
     ````
     */
    func controllerResumed() {
        nav = self.navigationController as? NavigationViewController
        nav.navDelegate = self
        nav.rightButton.setTitle(Theme.shared.doneTitle, for: .normal)
        nav.toggleButton()
    }
}

extension AlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // Number of Albums
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("\(logTitle!) Album Count : \(albumModel.count)")
        return albumModel.count
    }
    
    // Each Album Item
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "album", for: indexPath) as! AlbumPicsCollectionViewCell
        
        if albumView {
            cell.title.text = albumModel[indexPath.row].title
            if indexPath.row % 2 == 0 {
                if evenCounter % 2 == 0 {
                    cell.backgroundColor = Theme.shared.secondaryColorDark
                } else {
                    cell.backgroundColor = Theme.shared.secondaryColor
                }
                evenCounter+=1
            } else {
                if oddCounter % 2 == 0 {
                    cell.backgroundColor = Theme.shared.secondaryColorDark
                } else {
                    cell.backgroundColor = Theme.shared.secondaryColor
                }
                oddCounter+=1
            }
            cell.resetAlbumConstraints()
        } else {
            cell.title.text = photosModel[indexPath.row].title
            if cell.albumImage.image != nil {
                nav.showLoading(bool: false)
            }
            cell.albumImage.loadImage(photosModel[indexPath.row].thumbnailUrl) {
                if self.albumView {
                    cell.albumImage.image = UIImage(named: "default_album")?.withRenderingMode(.alwaysTemplate)
                }
                self.nav.showLoading(bool: false)
            }
            cell.setUpPhotosConstraints()
        }
    
        return cell
    }
    
    // Album Cell Size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight  {
            return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height/2)
        }
        return CGSize(width: collectionView.frame.width/2, height: collectionView.frame.height/4)
    }
    
    // Album Cell Spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // Album Cell Selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if albumView {
            nav.showLoading(bool: true)
            albumView = false
            ApiCore.shared.getApi(urlString: Theme.shared.photosUrl+String(albumModel[indexPath.row].id)) { (data, success) in
                if success {
                    do {
                        self.photosModel = try JSONDecoder().decode([PhotosModel].self, from: data)
                        DispatchQueue.main.async {
                            self.albumPicsCV.reloadData()
                            if self.albumModel.count > 0 {
                                let indexPath = IndexPath(item: 1, section: 0)
                                self.nav.toggleButton()
                                self.albumPicsCV.scrollToItem(at: indexPath, at: .top, animated: false)
                            }
                        }
                    } catch let err {
                        print("\(self.logTitle!) JSON ERROR : \(err)")
                    }
                } else {
                    self.nav.showLoading(bool: false)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            // Push to PhotoViewController
            let nextVC = PhotoViewController()
            nextVC.imageUrl = photosModel[indexPath.row].url
            nextVC.delegate = self
            nextVC.nav = nav
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}

