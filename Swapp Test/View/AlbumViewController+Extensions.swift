//
//  AlbumViewController+Extensions.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

extension AlbumViewController {
    
    /**
     Setup Layout
     ````
     All the layout elements are setup here
     ````
     */
    func setUpLayout() {
        view.backgroundColor = Theme.shared.secondaryColor
        
        nav = self.navigationController as? NavigationViewController
        nav.leftButton.isHidden = false
        nav.navDelegate = self
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        albumPicsCV = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        albumPicsCV.translatesAutoresizingMaskIntoConstraints = false
        albumPicsCV.backgroundColor = .clear
        albumPicsCV.register(AlbumPicsCollectionViewCell.self, forCellWithReuseIdentifier: "album")
        
        view.addSubview(albumPicsCV)
        
        albumPicsCV.delegate = self
        albumPicsCV.dataSource = self
        
        addConstraints()
    }
    
    /**
     Setup Constraints
     ````
     All the layout constraints are added here
     ````
     */
    func addConstraints() {
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": albumPicsCV]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(Theme.shared.navBarHeight!)-[v0]|", options: [], metrics: nil, views: ["v0": albumPicsCV]))
    }
    
    override func viewWillLayoutSubviews() {
        // print("ViewController Will Layout")
        albumPicsCV.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.controllerResumed()
    }
}
