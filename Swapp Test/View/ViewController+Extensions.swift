//
//  ViewController+Extensions.swift
//  Swapp Test
//
//  Created by Michael Fernandez on 24/01/19.
//  Copyright © 2019 Michael Fernandez. All rights reserved.
//

import UIKit

extension ViewController {
    
    /**
     Setup Layout
     ````
     All the layout elements are setup here
     ````
     */
    func setUpLayout() {
        
        view.backgroundColor = Theme.shared.secondaryColor
        
        nav = self.navigationController as? NavigationViewController
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        userCV = UICollectionView(frame: .zero, collectionViewLayout: layout)
        userCV.translatesAutoresizingMaskIntoConstraints = false
        userCV.register(UserCollectionViewCell.self, forCellWithReuseIdentifier: "user")
        userCV.backgroundColor = .clear
        userCV.delegate = self
        userCV.dataSource = self
        userCV.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        view.addSubview(userCV)
        
        addConstraints()
    }
    
    /**
     Setup Constraints
     ````
     All the layout constraints are added here
     ````
     */
    func addConstraints() {
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: [], metrics: nil, views: ["v0": userCV]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(Theme.shared.navBarHeight!)-[v0]|", options: [], metrics: nil, views: ["v0": userCV]))
    }
}
